# LIS 3781 Advanced Database Administration

## Chris Thornton


1. git init - creates an empty repository
2. git status - displays state of working directory and the staging area
3. git add -  adds files to the staging area
4. git commit - commits files to local repository
5. git push - publish local commits on remote server
6. git pull - fetches content from remote server
7. git diff - view differences between branches