-- Table person 
IF OBJECT_ID (N'dbo.person', N'U') IS NOT NULL DROP TABLE dbo.person; GO 
CREATE TABLE dbo.person ( 
per_id SMALLINT not null identity(1,1), per_ssn binary(64) NULL, per_salt binary(64) NULL, per_fname VARCHAR(15) NOT NULL, per_lname VARCHAR(30) NOT NULL, per_gender CHAR(1) NOT NULL CHECK (per_gender IN('m','f'), per_dob DATE NOT NULL, per_street VARCHAR(30) NOT NULL, per_city VARCHAR(30) NOT NULL, per_state CHAR(2) NOT NULL DEFAULT 'FL',
 per_zip int NOT NULL check (per_zip like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'), per_email VARCHAR(100) NULL, per_type CHAR(1) NOT NULL CHECK (per_type IN('c','s')), per_notes VARCHAR(45) NULL, 
 PRIMARY KEY (per_id), 
-- make sure SSNs and State IDs are unique 
CONSTRAINT ux_per_ssn unique nonclustered (per_ssn ASC) 
); GO 
