drop sequence seq_cus_id;
create sequence seq_cus_id
start with 1 
increment by 1
minvalue 1
maxvalue 10000;


drop table customer cascade constraints purge;
create table customer
(
    cus_id      number not null,
    cus_fname   varchar2(15) not null,
    cus_lname   varchar2(30) not null,
    cus_street  varchar2(30) not null,
    cus_city    varchar2(30) not null,
    cus_state   char(2) not null,
    cus_zip     number(9) not null,
    cus_phone   number(10) not null,
    cus_email   varchar2(100),
    cus_balance number(7,2),
    cus_notes   varchar2(255),
    constraint pk_customer primary key(cus_id)
);

drop sequence seq_com_id;
create sequence seq_com_id
start with 1 
increment by 1
minvalue 1
maxvalue 10000;

drop table commodity cascade constraints purge;
create table commodity
(
    com_id      number not null,
    com_name    varchar2(20),
    com_price   number(8,,2) not  null,
    cus_notes   varchar2(255),
    constraint pk_commodity primary key(com_id),
    constraint uq_com_name unique(com_name)
);


drop sequence seq_ord_id;
create sequence seq_ord_id
start with 1 
increment by 1
minvalue 1
maxvalue 10000;

drop table "order" cascade constraints purge;
create table "order"
(
    ord_id      number(4,0) not null,
    cus_id      number,
    com_id      number,
    ord_num_units   number(5,0) not null,
    ord_total_cost  number(8,2) not null,
    ord_notes   varchar2(255),
    constraint pk_order primary key(ord_id),
    constraint fk_order_customer
    foreign key (cus_id),
    references customer(cus_id),
    constraint fk_order_commodity
    foreign key (com_id),
    references commodity(com_id),
    constraint check_unit check(ord_num_units >0),
    constraint check_total check(ord_total_cost >0)
);


INSERT INTO customer VALUES (seq_cus_id.nextval, 'Beverly','Davis','123 big st.','Deroit','MI',4852,3135551212,'bdavis@123.com',11500.99,NULL);
INSERT INTO customer VALUES (seq_cus_id.nextval, 'Harley','Davidson','125 big st.','Deroit','MI',4852,3135551211,'name4@123.com',11545.99,NULL);
INSERT INTO customer VALUES (seq_cus_id.nextval, 'Bob','Davis','124 big st.','Deroit','MI',4852,3135551213,'name2@123.com',71500.99,NULL);
INSERT INTO customer VALUES (seq_cus_id.nextval, 'Bill','Davis','122 big st.','Deroit','MI',4852,3135551214,'name2@123.com',91500.99,NULL);
INSERT INTO customer VALUES (seq_cus_id.nextval, 'Jacob','Davis','121 big st.','Deroit','MI',4852,3135551215,'name1@123.com',15500.99,NULL);
commit;


INSERT INTO commodity VALUES (seq_com_id.nextval, 'DVD & Player', 149.99,NULL);
INSERT INTO commodity VALUES (seq_com_id.nextval, 'Cereal', 549.99,NULL);
INSERT INTO commodity VALUES (seq_com_id.nextval, 'Scrabble', 749.99,NULL);
INSERT INTO commodity VALUES (seq_com_id.nextval, 'Tums', 849.99,NULL);
INSERT INTO commodity VALUES (seq_com_id.nextval, 'Licorice', 949.99,NULL);
commit;


INSERT INTO "order" VALUES (seq_ord_id.nextval, 1, 1, 1, 1,NULL);
INSERT INTO "order" VALUES (seq_ord_id.nextval, 2, 2, 2, 2,NULL);
INSERT INTO "order" VALUES (seq_ord_id.nextval, 3, 3, 3, 3,NULL);
INSERT INTO "order" VALUES (seq_ord_id.nextval, 4, 4, 4, 4,NULL);
INSERT INTO "order" VALUES (seq_ord_id.nextval, 5, 5, 5, 5,NULL);
commit;

select * from customer;
select * from commodity;
select * from "order";
